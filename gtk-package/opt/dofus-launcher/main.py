import gi
import subprocess
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class AppWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Menu d'installation Dofus")
        self.set_border_width(15)

        hbox = Gtk.Box(spacing=7)
        self.add(hbox)

        btn = Gtk.Button.new_with_label("Installation")
        btn.connect("clicked", self.installation)
        hbox.pack_start(btn, True, True, 0)

        btn = Gtk.Button.new_with_label("Mise à jour")
        btn.connect("clicked", self.update)
        hbox.pack_start(btn, True, True, 0)

    def installation(self, btn):
        _subw = Installation()

    def update(self, btn):
        subprocess.call("/home/slowsaz/environment-test/gtk-program/package/opt/dofus-launcher/update.sh")

class Installation(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Choix de la distribution")
        self.set_border_width(15)

        hbox = Gtk.Box(spacing=5)
        self.add(hbox)

        btn = Gtk.Button.new_with_label("Ubuntu 16.04")
        btn.connect("clicked", self.main)
        btn.connect("clicked", self.ubuntu16)
        hbox.pack_start(btn, True, True, 0)

        btn = Gtk.Button.new_with_label("Ubuntu 18.04")
        btn.connect("clicked", self.main)
        btn.connect("clicked", self.ubuntu18)
        hbox.pack_start(btn, True, True, 0)

        btn = Gtk.Button.new_with_label("Ubuntu 20.04")
        btn.connect("clicked", self.main)
        btn.connect("clicked", self.ubuntu20)
        hbox.pack_start(btn, True, True, 0)

        btn = Gtk.Button.new_with_label("Debian 9")
        btn.connect("clicked", self.main)
        btn.connect("clicked", self.debian9)
        hbox.pack_start(btn, True, True, 0)

        btn = Gtk.Button.new_with_label("Debian 10")
        btn.connect("clicked", self.main)
        btn.connect("clicked", self.debian10)
        hbox.pack_start(btn, True, True, 0)
        self.show_all()

    def main(self, btn):
        subprocess.call("/opt/dofus-launcher/main.sh")

    def ubuntu16(self, btn):
        subprocess.call("/opt/dofus-launcher/ubuntu16.sh")

    def ubuntu18(self, btn):
        subprocess.call("/opt/dofus-launcher/ubuntu18.sh")

    def ubuntu20(self, btn):
        subprocess.call("/opt/dofus-launcher/ubuntu20.sh")

    def debian9(self, btn):
        subprocess.call("/opt/dofus-launcher/debian9.sh")

    def debian10(self, btn):
        subprocess.call("/opt/dofus-launcher/debian10.sh")

win = AppWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
